#!/bin/bash

currentip="~/currentip.txt"
oldip="~/oldip.txt"

didwegetip=$(curl --no-progress-meter -w "%{http_code}" --location "https://ipv4.icanhazip.com" -o $currentip)

if [ $didwegetip != "200" ]; then
        echo "failed to get current ip"
else

    	realip=$(<$currentip)
        echo "got the new ip"

        if cmp -s "$currentip" "$oldip" ; then
                echo "ip hasnt changed"
        else
            	printf "The ip has changed from %s to %s\n" "$(<$oldip)" "$realip"

                res1=$(curl --no-progress-meter -w "%{http_code}" --location "https://dynamicdns.park-your-domain.com/update?host=@&domain=chimbus.online&password=password&ip=$realip" -o /dev/null)
                res2=$(curl --no-progress-meter -w "%{http_code}" --location "https://dynamicdns.park-your-domain.com/update?host=@&domain=joshuahenley.com&password=password&ip=$realip" -o /dev/null)
                res3=$(curl --no-progress-meter -w "%{http_code}" --location "https://dynamicdns.park-your-domain.com/update?host=@&domain=ravvle.space&password=password&ip=$realip" -o /dev/null)

                if [[ $res1 != "200" && $res2 != "200" && $res3 != "200" ]]; then
                        echo "failed to update ip"
                else
                    	echo "ip updated"
                        cp $currentip $oldip
                fi

        fi
fi